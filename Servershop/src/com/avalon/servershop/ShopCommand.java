package com.avalon.servershop;

import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.trait.LookClose;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import com.avalon.utils.Lang;

public class ShopCommand implements CommandExecutor {
	
	
	public ServerShop plugin;
	
	public ShopCommand(ServerShop instance) {
		plugin = instance;
	}
	

	@Override
	public boolean onCommand(CommandSender sender, Command cmd,
			String commandLabel, String[] args) {
		    if (sender instanceof Player) {
		    	Player p = (Player) sender;
		    	
		    	// /shop
		    	
		    	if (args.length == 0) {
		    		if (plugin.getConfig().getBoolean("main.enableCommand")) {
		    	p.openInventory(plugin.slist);
		    	return false;
		    		} else {
		    			if (p.hasPermission("shop.command.bypass")) {
		    				p.openInventory(plugin.slist);
		    			} else {
		    			p.sendMessage(Lang.NO_PERMISSION.toString());
		    			}
		    		}
		    	return true;
		    	}
		    	
		    	
		    	// shop [1]
		    	else if (args.length == 1) {
		    		
		    		
		    		if (args[0].equalsIgnoreCase("reload")) {
		    			if (p.hasPermission("shop.reload")) {
		    			
		    			plugin.reloadConfig();
		    			plugin.reloadShops();
		    			p.sendMessage(Lang.RELOAD.toString());
		    			return true;
		    			
		    		} else {
		    			p.sendMessage(Lang.NO_PERMISSION.toString());
		    		}
		    		}
		    		
		    		
		    		// shop info
		    		
		    		if (args[0].equalsIgnoreCase("info")) {
		    			if (p.getItemInHand().getType() != Material.AIR ) {
		    				p.sendMessage(ChatColor.GRAY + "Material: " + ChatColor.GREEN + p.getItemInHand().getType());
		    				p.sendMessage(ChatColor.GRAY + "Data value: " + ChatColor.GREEN + p.getItemInHand().getDurability());
		    				return true;
		    			}
		    			
		    		}
		    		
		    		// shop help
		    		if (args[0].equalsIgnoreCase("help")) {
		    			p.sendMessage(ChatColor.GREEN + "/-/=====/-/[Shop Help]/-/=====/-/");
		    			p.sendMessage(ChatColor.GRAY + "/shop reload" + ChatColor.GREEN + " - Reload the plugin files.");
		    			p.sendMessage(ChatColor.GRAY + "/shop info" + ChatColor.GREEN + " - Get some information about the item your holding.");
		    			p.sendMessage(ChatColor.GRAY + "/shop createnpc <name> <shop>" + ChatColor.GREEN + " - Create a shop NPC at your location with the given <name> linked to <shop>");
		    			p.sendMessage(ChatColor.GRAY + "/shop version" + ChatColor.GREEN + " - Get some informations about the plugin.");
		    			p.sendMessage(ChatColor.GREEN + "/-/=====/-/[Shop Help]/-/=====/-/");
		    			return true;
		    			
		    		}
		    		// shop version.
		    		if (args[0].equalsIgnoreCase("version")) {
		    			p.sendMessage(ChatColor.GREEN + "/-/=====/-/[ServerShop-GUI]/-/=====/-/");
		    			p.sendMessage(ChatColor.GRAY + "This server is running ServerShop-GUI version " + plugin.getDescription().getVersion());
		    			p.sendMessage(ChatColor.GRAY + "BukkitDev link: http://dev.bukkit.org/bukkit-plugins/servershop-gui/");
		    			return true;

		    			}
		    		
		    		if (args[0].equalsIgnoreCase("createnpc")) {
		    			if (p.hasPermission("shop.npc.create")) {
		    			p.sendMessage(ChatColor.GREEN + "/-/=====/-/[ServerShop-GUI]/-/=====/-/");
		    			p.sendMessage(ChatColor.GRAY + "Please use /shop createnpc <name of the npc> <shop name>");
		    			p.sendMessage(ChatColor.GRAY + "Please note: You cannot spawn more than one npc with the same name.");
		    			return true;
		    			} else {
		    				p.sendMessage(Lang.NO_PERMISSION.toString());
		    				return false;
		    			}
		    			}

		    		if (plugin.ShopSelect.containsKey(args[0])) {
		    			int ID = plugin.ShopSelect.get(args[0]);
		    			if (p.hasPermission("shop.open."+ args[0])) {
		    			p.openInventory(plugin.shops.get(ID));
		    			return true;
		    			}
		    			return true;
		    		}
		    	}   
		    	
		    	else if (args.length == 3) {		    		
		    		if (args[0].equalsIgnoreCase("createnpc")) {
		    			if (p.hasPermission("shop.npc.create")) {
		    				if (plugin.npc != null) {
		    					if (plugin.ShopSelect.containsKey(args[2])) {
		    						String npcname = ChatColor.translateAlternateColorCodes('&', args[1]);
		    						NPC shop = plugin.npc.getNPCRegistry().createNPC(EntityType.PLAYER, npcname );
		    						plugin.npc.getTraitFactory().addDefaultTraits(shop);
					    			shop.faceLocation(p.getLocation());
					    			shop.addTrait(LookClose.class);
					    			shop.setProtected(true);
					    			shop.spawn(p.getLocation());

					    			plugin.getConfig().set("NPC." + shop.getId(), args[2]);
					    			plugin.saveConfig();
					    			plugin.reloadConfig();

					    			p.sendMessage(Lang.NPC_SPAWNED.toString());
					    			return true;
		    					} else {
		    						p.sendMessage(Lang.NPC_ERROR_NOSHOP.toString().replace("%shop%", args[2]));
		    						return true;
		    					}
		    				} else { p.sendMessage(Lang.NPC_NOT_ENABLED.toString()); return true; }
		    			} else {
		    				p.sendMessage(Lang.NO_PERMISSION.toString());
		    				return true;
		    			}
		    		}
		    	}
		    }
		    return true;
}
}
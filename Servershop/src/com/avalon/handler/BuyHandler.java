package com.avalon.handler;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.avalon.servershop.BlockBuyableCommands;
import com.avalon.servershop.ServerShop;
import com.avalon.utils.Lang;
import com.avalon.utils.util;

public class BuyHandler {

	private ItemStack i;
	private Player p;
	private String type;
	private String price;
	private String PriceType;
	private String PermissionNode;
	private List<String> lore;
	private String Command;
	private String shopname;	
	
	public BuyHandler(ItemStack item, Player p, String InventoryName) {
		this.shopname = InventoryName;
		this.i = item;
		this.p = p;
		this.setType();
		this.setPrice();
		this.setPriceType();
		this.setLore(p);
		
	}
	
	private void setType() {		
		String lore = this.i.getItemMeta().getLore().get(0);
		String[] loreargumente = lore.split(" ");
		this.type = loreargumente[1];
		}
		
	private void setPrice() {
		
		
		if (this.type.equals("Item")) {
		String lore = this.i.getItemMeta().getLore().get(2);
		String[] loreargumente = lore.split(" ");
		this.price = ChatColor.stripColor(loreargumente[1]);
		}
		else if (this.type.equals("Permission")) {
			this.setPermissionNode();
			String lore = this.i.getItemMeta().getLore().get(2);
			String[] loreargumente = lore.split(" ");
			this.price = ChatColor.stripColor(loreargumente[1]);
		}
		
		else if (this.type.equals("Command")) {
			this.setCommndNode();
			String lore = this.i.getItemMeta().getLore().get(2);
			String[] loreargumente = lore.split(" ");
			this.price = ChatColor.stripColor(loreargumente[1]);
		}
		
		else if (this.type.equals("PlayerCommand")) {
			this.setPlayerCommndNode();
			String lore = this.i.getItemMeta().getLore().get(2);
			String[] loreargumente = lore.split(" ");
			this.price = ChatColor.stripColor(loreargumente[1]);
		}
	}
		
	private void setLore(Player p) {
		if (this.i.hasItemMeta()) {
			if (this.i.getItemMeta().getLore() != null) {
				this.lore = this.i.getItemMeta().getLore();

				List<String> remList = new ArrayList<String>();
				for(int i=0;i<=4;i++){
				remList.add(lore.get(i));
				}
				lore.removeAll(remList);
			}
		}
	}
	
	private void setPriceType() {
		
		if (this.type.equals("Item")) {
		String lore = this.i.getItemMeta().getLore().get(3);
		String[] loreargumente = lore.split(" ");
		this.PriceType = loreargumente[1];
		return;
		}
		else if (this.type.equals("Permission")) {
			String lore = this.i.getItemMeta().getLore().get(3);
			String[] loreargumente = lore.split(" ");
			this.PriceType = loreargumente[1];
			return;
		}
		
		else if (this.type.equals("Command")) {
			String lore = this.i.getItemMeta().getLore().get(3);
			String[] loreargumente = lore.split(" ");
			this.PriceType = loreargumente[1];
			return;
		}
		
		else if (this.type.equals("PlayerCommand")) {
			String lore = this.i.getItemMeta().getLore().get(3);
			String[] loreargumente = lore.split(" ");
			this.PriceType = loreargumente[1];
			return;
		}
		
	}
	
	private void setPermissionNode() {
		String displayname = ChatColor.stripColor(i.getItemMeta().getDisplayName());
		this.PermissionNode = ServerShop.PermissionShopItems.get(this.shopname + displayname);
		return;
		
	}
	
	private void setCommndNode() {
		String displayname = ChatColor.stripColor(i.getItemMeta().getDisplayName());
		this.Command = ServerShop.CommandShopItems.get(this.shopname + displayname);
		return;
		
	}
	
	private void setPlayerCommndNode() {
		String displayname = ChatColor.stripColor(i.getItemMeta().getDisplayName());
		this.Command = ServerShop.PlayerShopItems.get(this.shopname + displayname);
		return;
		
	}	
	public boolean Pay() {
		
		if (this.type.equals("Item")) {
			if (!util.hasSpot(p)) { return false; }
			 if (this.PayPrice(this.PriceType, this.price)) {
					ItemStack item = new ItemStack(i);						
					if (this.lore != null) {
						ItemMeta meta = item.getItemMeta();
						meta.setLore(this.lore);
						item.setItemMeta(meta);
					}
					
					this.p.getInventory().addItem(item);
					this.lore.clear();
					util.logToFile(p.getName() + " bought " + this.i.getType() + " x" + this.i.getAmount() + " for " + this.price + "[" + this.PriceType + "]",this.shopname + "-bought.txt");
					return true;
			 }
			return true;
		}
				
		else if (this.type.equals("Permission")) {
			
			
			if (ServerShop.PermissionCooldownPlayer.containsKey(p.getName())) {
				   String displayname = ChatColor.stripColor(i.getItemMeta().getDisplayName());
				   int CDtime = ServerShop.PermissionShopCooldown.get(this.shopname+displayname);
				   long secondsLeft = ((ServerShop.PermissionCooldownPlayer.get(p.getName())/1000)+CDtime) - (System.currentTimeMillis()/1000);
		            if(secondsLeft>0) {
		                p.sendMessage(Lang.COOLDOWN_MESSAGE.toString().replace("%seconds%", ""+secondsLeft));
		                return false;
		            }
		        }
			
			
			String player = p.getName();
			String world = null;
			if (this.PermissionNode.contains(":")) {
			
			String firstPerm = this.PermissionNode.replace(":", " ");
			
			int i = firstPerm.indexOf(' ');
			String perm = firstPerm.substring(0, i);
			
			if (!p.hasPermission(perm)) {
				 if (this.PayPrice(this.PriceType, this.price)) {
					 
					   for (String permission : this.PermissionNode.split(":")) {
						   ServerShop.permission.playerAdd(world, player, permission);
						   util.logToFile(p.getName() + " bought " + this.PermissionNode + " for " + this.price + "[" + this.PriceType + "]",this.shopname + "-bought.txt");
					   }
					   p.sendMessage(Lang.PERMISSION_BOUGHT.toString());
					   return true;
				   }
				 } else {
					 	p.sendMessage(Lang.PERMISSION_ALREADY_BOUGHT.toString());
						return false;
					}
			} 
			
			else {
				String perm = this.PermissionNode;
				if (!p.hasPermission(perm)) {
					if (this.PayPrice(this.PriceType, this.price)) {
						ServerShop.permission.playerAdd(world, player, perm);
						   util.logToFile(p.getName() + " bought " + this.PermissionNode + " for " + this.price + "[" + this.PriceType + "]",this.shopname + "-bought.txt");
						   p.sendMessage(Lang.PERMISSION_BOUGHT.toString());
						   return true;
					} 
				}
				else {
					p.sendMessage(Lang.PERMISSION_ALREADY_BOUGHT.toString());
					return false;
				}
			}
			}
		
			else if (this.type.equals("Command")) {
			
				   if (ServerShop.CommandShopCooldownPlayer.containsKey(p.getName())) {
					   String displayname = ChatColor.stripColor(i.getItemMeta().getDisplayName());
					   int CDtime = 0;
					   if (ServerShop.CommandShopCooldown.get(this.shopname+displayname) != null) {
					   CDtime = ServerShop.CommandShopCooldown.get(this.shopname+displayname);
					   }
					   long secondsLeft = ((ServerShop.CommandShopCooldownPlayer.get(p.getName())/1000)+CDtime) - (System.currentTimeMillis()/1000);
			            if(secondsLeft>0) {
			            	p.sendMessage(Lang.COOLDOWN_MESSAGE.toString().replace("%seconds%", ""+secondsLeft));
			                return false;
			            }
			        }
				
			if (this.PayPrice(this.PriceType, this.price)) {
					
				for (String cmd : this.Command.split(":")) {
					Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), cmd.replaceAll("%player%", p.getName()));
					ServerShop.CommandShopCooldownPlayer.put(p.getName(), System.currentTimeMillis());
					util.logToFile(p.getName() + " bought " + cmd + " for " + this.price + "[" + this.PriceType + "]",this.shopname + "-bought.txt");
				   }
				return true;
			}
		}
		
		
		
		
		else if (this.type.equals("PlayerCommand")) {
			
			
			if (ServerShop.PlayerShopCooldownPlayer.containsKey(p.getName())) {
				   String displayname = ChatColor.stripColor(i.getItemMeta().getDisplayName());
				   int CDtime = 0;
				   if (ServerShop.PlayerShopCooldown.get(this.shopname+displayname) != null) {
				   CDtime = ServerShop.PlayerShopCooldown.get(this.shopname+displayname);
				   }
				   long secondsLeft = ((ServerShop.PlayerShopCooldownPlayer.get(p.getName())/1000)+CDtime) - (System.currentTimeMillis()/1000);
		            if(secondsLeft>0) {
		            	p.sendMessage(Lang.COOLDOWN_MESSAGE.toString().replace("%seconds%", ""+secondsLeft));
		                return false;
		            }
		        }
			
			
			if (this.PayPrice(this.PriceType, this.price)) {
				
				for (String cmd : this.Command.split(":")) {
					BlockBuyableCommands.execute.add(p.getName());
					p.chat(cmd);
					BlockBuyableCommands.execute.remove(p.getName());
					util.logToFile(p.getName() + " bought " + cmd + " for " + this.price + "[" + this.PriceType + "]",this.shopname + "-bought.txt");
				   }
				return true;
			}
		}
		
		else {
			return false;
		}
		return true;
	}
	
	
	
	// Method to pay the set Pricetype and amount.
	
	private boolean PayPrice(String Pricetype, String amount) {
		
		if (amount.equals("Free")) {
			return true;
		}
		else if (amount.equals("Unable")) {
			return false;
		}
		
		switch(Pricetype) {
		case "Money":
			if (util.payMoney(p, Float.parseFloat(this.price))) {
				return true;
			} else { return false; }
		case "EXP":
			int lvl = new Double(this.price).intValue();
			if (util.payEXP(p, lvl)) {
				return true;
			} else { return false; }

		case "Item":
			int IPA = new Double(this.price).intValue();
			if (util.payitem(p, IPA)) {
				return true;
			} else { return false; }
			
		case "Token":
			int token = new Double(this.price).intValue();
			if (util.payToken(p, token)) {
				return true;
			} else { return false; }			
		default:
			return false;
		}
	}
	
	
}

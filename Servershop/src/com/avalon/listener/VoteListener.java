package com.avalon.listener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.avalon.servershop.ServerShop;
import com.avalon.token.TokenUtils;
import com.avalon.utils.Lang;
import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;

public class VoteListener implements Listener {
	
	
	public ServerShop plugin;
	public VoteListener(ServerShop instance) {
		plugin = instance;
	}
	
	  @EventHandler(priority=EventPriority.NORMAL)
	  public void onVote(VotifierEvent event)
	  {
		Vote vote = event.getVote();
	    String user = vote.getUsername();
	    Player p = Bukkit.getPlayerExact(user);
	    String amount = "" +plugin.getConfig().getInt("Vote.TokenAmount");
	    if (p != null) {
	    TokenUtils.giveToken(p.getName(), plugin.getConfig().getInt("Vote.TokenAmount"));
	    p.sendMessage(Lang.VOTE_MESSAGE.toString().replace("%amount%", amount));
	    return;
	    }
}
}
package com.avalon.listener;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import com.avalon.handler.BuyHandler;
import com.avalon.handler.SellHandler;
import com.avalon.servershop.ServerShop;
import com.avalon.utils.Lang;

public class InventoryClick implements Listener {

	  public static ServerShop plugin;

	  public InventoryClick(ServerShop instance)
	  {
	    plugin = instance;
	  }
	  	  
	  @EventHandler
	  public void ShopClickEvent(InventoryClickEvent event) {
		  
		  String InventoryName = ChatColor.stripColor(event.getInventory().getName());
		  if (event.getInventory().getName().equals(plugin.slist.getName())) {
			  event.setCancelled(true);
			  if (event.getCurrentItem() != null && event.getCurrentItem().getType() != Material.AIR) {
				  Player p = (Player)event.getWhoClicked();
				  if (plugin.getConfig().getBoolean("main.EnableClickSound")) {
				  p.playSound(p.getLocation(), Sound.WOOD_CLICK, 3, 1);
				  }
				  p.closeInventory();
				  String ShopName = ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName());
				  plugin.debug("Clicked on Shopname: " + ShopName);
				  if (plugin.ShopSelect.containsKey(ShopName)) {
				  int ID = plugin.ShopSelect.get(ShopName);
				  if (p.hasPermission("shop.open."+ShopName)) {
				  p.openInventory(plugin.shops.get(ID));
				  return;
				  } else {
					  p.sendMessage(Lang.NO_PERMISSION.toString());
					  return;
				  }
				  } else {
					  plugin.log.warning("[ServerShop-GUI] Player " + p.getName() + " tried to open shop[" + ShopName + "]. But no config exists for this shop."
					  		+ " Please create a shop configuration (" + ShopName + ".yml)");
				  }
			  }
			  return;
		  }
		  
		  if (plugin.ShopSelect.containsKey(InventoryName)) {
			  event.setCancelled(true);
			  
			  if ((event.getCurrentItem() != null) && (event.getCurrentItem().getType() != Material.AIR)) {
				  
				  ItemStack clicked = event.getCurrentItem();
				  String dn = clicked.getItemMeta().getDisplayName();
				  Player p = (Player)event.getWhoClicked();
				  if (plugin.getConfig().getBoolean("main.EnableClickSound")) {
				  p.playSound(p.getLocation(), Sound.WOOD_CLICK, 3, 1);
				  }
				  
				  if (event.isLeftClick() && event.getRawSlot() == event.getSlot()) {
					  if (dn != null) {
					  if (dn.equals(ChatColor.YELLOW + "Return")) {
						  p.closeInventory();
						  p.openInventory(plugin.slist);
						  return;
					  }
					  }
					    if (plugin.getConfig().getBoolean("main.EnableSlotPermissions")) {
					    	
					    	if (!p.hasPermission("shop." + InventoryName + "." + event.getSlot())) {
					    		p.sendMessage(Lang.NO_SLOT_PERMISSION.toString());
					    		return;
					    	}
					    	
					    }
					  
					  
						BuyHandler LeftClickHandler = new BuyHandler(clicked, p, InventoryName);
						
						if (LeftClickHandler.Pay()) {
							return;
						} else {
							return;
						}
				  }  
				  
				  else if (event.isRightClick() && event.getRawSlot() == event.getSlot()) {
					  
					  
					  if (plugin.getConfig().getBoolean("main.EnableSlotPermissions")) {
					    	
					    	if (!p.hasPermission("shop." + InventoryName + "." + event.getSlot())) {
					    		p.sendMessage(Lang.NO_SLOT_PERMISSION.toString());
					    		return;
					    	}
					    	
					    }
					  
					  
					  SellHandler RightClickHandler = new SellHandler(clicked, p, InventoryName);
						if (RightClickHandler.Sell()) {
							return;
						} else {
							return;
						}
				  }
				  
				  else {return;}
			  } 
		  }
	  }
}